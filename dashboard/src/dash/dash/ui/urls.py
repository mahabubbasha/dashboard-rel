"""
URL config for Junos Release Dashboard

Mahabub Basha, mbasha@juniper.net, 2013-11-14

Copyright (c) 2013, Juniper Networks, Inc.
All rights reserved.
"""
import os

from django.conf import settings
from django.conf.urls.defaults import *
urlpatterns = patterns('dash.ui.views',
    # Agenda page
    url(r'^/agenda$','agenda',name='agenda')
)

# To serve static URL's.
urlpatterns += patterns('',
    (r'^/(?P<path>(inc|images).*)$', 'django.views.static.serve',{'document_root': os.path.dirname(__file__), 'show_indexes': True}),
)

