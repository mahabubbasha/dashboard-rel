"""
Base functionality for Junos Release Dashboard.

Mahabub Basha, mbasha@juniper.net, 2013-11-14

Copyright (c) 2013, Juniper Networks, Inc.
All rights reserved.
"""

from django.conf import settings
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.core.urlresolvers import reverse, NoReverseMatch

def context_processor(request):
    """ Adds commonly used information to the request context. """
    context = \
        {
         'version': settings.VERSION,
         'release_date': settings.RELEASE_DATE,
         'is_mozilla': request.META.get('HTTP_USER_AGENT', '').find('Gecko/') > 0,
        }
    return context

