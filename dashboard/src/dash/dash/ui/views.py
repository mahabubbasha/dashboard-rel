from django.http import HttpResponse, HttpResponseRedirect, \
                        HttpResponseNotFound
from django.shortcuts import render_to_response
from django.template import loader
from django.template.context import RequestContext

def agenda(request):
    return render_to_response('ui/agenda.html', {}, RequestContext(request, {
            'title': 'Agenda page',
    }))
